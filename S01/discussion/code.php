<?php 
// $trial = "hello World";
//[Section] Commnents
// Comments are part of the code that gets ignore by the language.
//Comments are meant to describe the algorithm of the written code.
/*
    there are two types of comments:
        - the single-line comment denoted by two forward slashes(//)
        - the multi-line comment denoted by a slash and asterisk(/*)
*/ 

// Section - variables
// variables are used to contain data.
// variable are named location for the stored value.
//variable are defined using the dollar ($) notation before the name of the variable

$name = "John Smith";
$email = "johnsmith@gmail.com";

// [Section] - Constants
// Constants used to hold data that are meant to be read-only.
// Constants are defined using the defined() function.
            /*
                Syntax: 
                    define('variableName', valueOfTheVariable);
            */ 

define('PI', 3.1416);

// Reassignment of a variable
$name = 'Will Smith';

// [Section] - Data Types

// Strings

$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country;

// Reassing the $address
$address = "$state, $country";

//Integers
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

//Boolean
$hasTravelAbroad = false;
$haveSymptoms = true;

//Null
$spouse = null;
$middle = null;

//[Section]
//Array
$grades = array(98.7, 92.1, 90.2, 94.6);

// objects
$gradesObj = (object)[
    'firstGrading' => 98.7,
    'secondGrading' => 92.1,
    'thirdGrading' => 90.2,
    'fourthGrading' => 94.6
];

$personObj = (object)[
    'fullName' => 'John Smith',
    'isMarried' => false,
    'age' => 35,
    'address' => (object)[
        'state' => 'New York',
        'country' => 'United States of America'
    ],
    'contacts' => array('09123456789', '0987654321')
];

//Section - Operators
//Assignment Operators(=)

//this operator is used to assign and reassign value/s of a variable

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// [Section] Functions
// Functions are used to make reusable code.

function getFullName($firstName, $middleInitial, $lastName){
    // you can add here your algorithm

    return "$lastName, $firstName $middleInitial";

}

//[Section] - Selection Control Structures
// Selection control structures are used to make code execution dynamic according to predefined conditions
    // if-elseif-else Statement

function determineTyphoonIntensity($windSpeed){
    // if-elseif-else Condition
    if($windSpeed <30){
        return 'Not a typhoon yet.';
    } else if($windSpeed <= 61){
        return ' tropical depression detected.';
    } else if($windSpeed <= 88){
        return ' tropical storm detected.';
    } else if($windSpeed <= 117){
        return ' Severe Tropical Storm detected.';
    } else {
        return 'Typhoon detected.';
    }
}

// Conditional (Ternary Opearator)
function isUnderAge($age){
    return ($age < 18) ? true: false;
}

//Switch Statement
function determineComputreUser($computerNumber){
    switch($computerNumber){
        case 1: 
            return 'Linus Torvalds';
            break;
        case 2:
            return 'Steve Jobs';
            break;
        case 3:
            return 'Sid Meir';
            break;
        case 4:
            return 'Onel de Guzman';
            break;
        case 5:
            return 'Christian Salvador';
            break;
        default:
            return $computerNumber. 'is out of bounds.';
            break;
    }
}

// Try-Catch_Finaly Statement

function greeting($str){
    try{
        // Attempt to execute the code
        if(gettype($str) == "string"){
            // If sucessful
            echo $str;
        } else {
            // If not, it will return "Oops!"
            throw new Exception("Oops!");
        }
    }
    catch (Exception $e){
        // Catch the error within "try"
        echo $e -> getMessage();
    }
    finally{
        // Continue execution of code regardless of sucess and failure of code execution in the 'try' block.
        echo "I did it again!";
    }
}

?> 