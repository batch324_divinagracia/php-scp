<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S01: PHP Basics and Selection Control structure</title>
    </head>
    
    <body>
        <h1>Echoing Values</h1>

        <!-- using single quaote for the echo -->
        <p><?php echo 'Good day $name! Your given email is $email!'; ?></p>

        <!-- using double quote for the echo -->
        <p><?php echo "Good day $name! Your given email is $email!"; ?></p>

        <p><?php echo PI; ?></p>
        <p><?php echo $name; ?></p>
        
        <h1>Data Types</h1>
        <h3>String</h3>
        <p><?php echo $state; ?></p>
        <p><?php echo $country; ?></p>
        <p><?php echo $address; ?></p>

        <h3>Integers</h3>
        <p><?php echo $age; ?></p>
        <p><?php echo $headcount; ?></p>

        <h3>Float</h3>
        <p><?php echo $grade; ?></p>
        <p><?php echo $distanceInKilometers; ?></p>

        <h3>Boolean</h3>
        <!-- Normal echoinh of boolean variables will not make it visiblel to the webpage -->
        <p><?php echo var_dump($hasTravelAbroad); ?></p>
        <p><?php echo var_dump($haveSymptoms); ?></p>

        <h3>Null</h3>
        <p><?php echo var_dump($spouse); ?></p>
        <p><?php echo $middle; ?></p>

        <h3>Arrays</h3>
        <p><?php echo $grades[3]?></p>
        <p><?php echo print_r($grades)?></p>
        
        <h3>Objects</h3>
        <p><?php echo print_r($gradesObj); ?></p>
        <p><?php echo $gradesObj->secondGrading; ?></p>
        <p><?php echo $personObj->address->state; ?></p>
        <p><?php echo $personObj->contacts[1]; ?></p>

        <h3>gettype()</h3>
        <p><?php echo gettype($state); ?></p>
        <p><?php echo gettype($age); ?></p>
        <p><?php echo gettype($grade); ?></p>
        <p><?php echo gettype($hasTravelAbroad); ?></p>
        <p><?php echo gettype($spouse); ?></p>
        <p><?php echo gettype($grades); ?></p>
        <p><?php echo gettype($gradesObj); ?></p>

        <h1>Operators</h1>
        <h3>Assignment Operator</h3>
        <p>X: <?php echo $x; ?></p>
        <p>Y: <?php echo $y; ?></p>

        
        <p>is Legal Age:  <?php echo var_dump($isLegalAge); ?></p>
        <p>is Registered:  <?php echo var_dump($isRegistered); ?></p>

        <h3>Arithmetic Operators</h3>
        <p>Sum: <?php echo $x + $y; ?></p>
        <p>Difference: <?php echo $x - $y; ?></p>
        <p>Product: <?php echo $x * $y; ?></p>
        <p>Quotient: <?php echo $x / $y; ?></p>

        <h3>Equality Operators</h3>
        <p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
        <p>Strict Operator: <?php echo var_dump($x === '1342.14'); ?> </p>

        <h3>Inequality Operators</h3>
        <p>Loose Inequality: <?php echo var_dump($x != '1342.14'); ?></p>
        <p>Strict Inequality: <?php echo var_dump($x !== '1342.14'); ?></p>
        <!-- Strict equality/ inequality is preferred sos that we can check both of the value and the data type given. -->

        <h3>Greater/Lesser Operators</h3>
        <p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
        <p>Is Greater: <?php echo var_dump($x > $y); ?></p>

        <p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
        <p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>

        <h3>Logical Operators</h3>
        <!-- Logical Operators are used to verify whether an expression or group of expression are either true or false -->

        <p>OR operator: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>

        <p>AND operator: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>

        <p>NOT operator: <?php echo var_dump(!$isLegalAge); ?></p>

        <h1>Functions</h1>
        <p><?php echo getFullName('John', 'D.', 'Smith');  ?></p>

        <h1>Selection Control Structure</h1>

        <h3>If-Else If- Else Statement</h3>
        <p><?php echo determineTyphoonIntensity(75)?></p>

        <h2>Ternary Sample (isUnderAge?)</h2>
        <p>78: <?php var_dump(isUnderAge(78)); ?></p>
        <p>17: <?php var_dump(isUnderAge(17)); ?></p>

        <h2>Switch</h2>

        <p><?php echo determineComputreUser(5); ?></p>

        <h2>Try-Catch-Finally</h2>

        <p><?php echo greeting(12); ?></p>
 
    </body>
</html>