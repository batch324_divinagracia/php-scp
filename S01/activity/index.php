<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activity S01</title>
    </head>

    <body>
        <h1>Activty S01</h1>
        <h3>Activity 1</h3>
        <p><?php echo getFullAdress('Philippines', 'Quezon City', 'Metro Manila', '3F Caswynn Bldg., Timog Avenue'); ?></p>
        <p><?php echo getFullAdress('Philippines', 'Makati City', 'Metro Manila', '3F Enzo Bldg., Buendia Avenue'); ?></p>

        <h3>Activity 2</h3>
        <p><?php echo getLetterGrade(87)?></p>
        <p><?php echo getLetterGrade(94)?></p>
        <p><?php echo getLetterGrade(74)?></p>


    </body>

</html>