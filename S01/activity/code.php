<?php 

function getFullAdress($country, $city, $province, $specificAdress){

    return "$specificAdress, $city, $province, $country ";
}

function getLetterGrade($grade){
    if($grade < 75){
        return "$grade is equavalent to D";
    } else if ($grade >= 75 && $grade <= 76){
        return "$grade is equavalent to C-";
    } else if($grade >= 77 && $grade <= 79){
        return "$grade is equvalent to C";
    } else if($grade >= 80 && $grade <= 82){
        return "$grade is equavalent to C+";
    } else if($grade >= 83 && $grade <= 85){
        return "$grade is equavalent to B-";
    } else if($grade >= 86 && $grade <= 88){
        return "$grade is equavalent to B";
    } else if($grade >= 89 && $grade <= 91){
        return "$grade is equavalent to B+";
    } else if($grade >= 92 && $grade <= 94){
        return "$grade is equavalent to A-";
    } else if($grade >= 95 && $grade <= 97){
        return "$grade is equavalent to A";
    } else if($grade >= 98 && $grade <= 100){
        return "$grade is equavalent to A+";
    } else{
        return "Invalid Grade";
    } 
}




?>