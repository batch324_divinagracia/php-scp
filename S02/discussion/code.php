<?php 
//[Section] Repetition Control Structures
    // It is used to execute code multiple tiems.

    //While loop
        // A while loop takes a single condition.

function whileLoop(){
    $count = 5;

    while($count !== 0){
        echo $count.'<br/>';
        // echo $count;
        $count--;
    }
}

// function whileLoop1($count){

//     while($count !== 0){
//         echo $count.'<br/>';
//         $count--;
//     }
// }

// Do-while Loop
    // A do-while loop works a lot like the while loop. the only difference is that,  do-while guarantee that the code will run/execute at least once.

function doWhileLoop(){
    $count = 20;

    do {
        echo $count.'<br/>';
        $count--;
    } while ($count > 20);
}

    // For Loop
    /*
        for (initial value; condition; iteration){
            code block
        }
    */

function forLoop(){
    for($count = 0; $count <= 20; $count++){
        echo $count.'<br/>';
    }
}

// Continue and Break Statements
    // "Continue" is a keyword that allows the code to go the next loop without finish the current code block.
    // "Break" on the other hand is a keyword that stop the execution of the current loop.

function modifiedForLoop(){
    for ($count = 0; $count <= 20; $count++){
        // if the count is divisible by 2, do this 
        if ($count %2 === 0){
            continue;
        }
        // if not just continue the iteration
        echo $count.'<br/>';
        // if the count is greater than 10, do this.
        if($count > 10){
            break;
        }
    }
}

// function activity1(){
//     for ($count = 1; $count <= 100; $count++){
//         if ($count %5 === 0){
//             echo $count.'<br/>';
//             continue;
//         }
//     }
// }

// [Section] Array Manipulation
    // An array is a kind of variable that can hold more than one value.
    // array are declared using array() function or square brackets '[]'.
        // In the earlier version of php, we cannot used [], but as of the 5.4 we can use the short array syntax which replace array() with [].


$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927');
// Before the php 5.4
$studentNumbers = ['2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927'];
// After the php 5.4

// Simple Array
$grades = [98.5, 94.3, 89.2, 90.1]; 
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
$tasks = [
    'drink html',
    'eat php',
    'inhale css',
    'bake javascript'
];

// Associative Array
    // Associative Array differ from numeric array in the sense that associative arrays uses decriptive names in naming the elements/ values (key => value pair).
    // double arrow Operator (=>) - an assignment operator that is commonly used in the creation of associative array.

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-Dimensional Array
    // Two dimensional array is commonly used in image processing, good example of this is our viewing screen that uses multideminsional array of pixels.

$heroes = [
    ['iron man', 'thor', 'hulk'],
    ['wolvarine', 'cyclops', 'jean grey'],
    ['batman', 'superman', 'wonder woman']
];

// Two-Dimensional Associative Array
$ironManPowers = [
    'regular' => ['repulsor blast', 'rocket punch'],
    'signature' => ['unibeam']
];

//Array Iterative method: foreach();
// foreach($heroes as $hero){
// 	print_r($hero);
// }

//[Section] Array Mutators
//Array Mutators modify the contents of an array.

//Array Sorting

$sortedBrands =  $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);



?>