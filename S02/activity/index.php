<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activity S02</title>
    </head>

    <body>
        <h1>Activity S02</h1>

        <h3>Activit 1</h3>

        <?php divisibleByFive(); ?>

        <h3>Activity 2</h3>
        <?php array_push($students, 'John Smith'); ?>
        <p> <?php print_r($students);  ?> </p>

        <p><?= count($students); ?></p>

        <?php array_push($students, 'Jane Smith'); ?>
        <p> <?php print_r($students);  ?> </p>

        <p><?= count($students); ?></p>

        <?php array_shift($students); ?>
        <p><?php print_r($students); ?></p>

        <p><?= count($students); ?></p>

        
    </body>

</html>