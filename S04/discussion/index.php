<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S04: Access Modifiers and Encapsulation</title>
    </head>

    <body>
        <h1>Access Modifiers</h1>

        <p><?php //$building->name = 'Caswynn Building;'?></p>

        <p><?php var_dump($building); ?></p>
        <p><?php echo $building->getName(); ?></p>
        <p><?php $building->setName('Caswynn Building');?></p>
        <p><?php echo $building->getName(); ?></p>
        <p><?php echo $building->getFloors(); ?></p>

        <p><?php var_dump($kopiko); ?></p>

        <p><?= $milk->getName(); ?></p>
        <?php $milk->setName('Bear Brand')?>
        <p><?= $milk->getName(); ?></p>
        <p><?= $kopiko->getName(); ?></p>

        <p><?php echo $condominium->getName(); ?></p>

        
    </body>

</html>