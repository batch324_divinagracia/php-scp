<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S03: Classes and Objects</title>
    </head>

    <body>
        <h1>Objects from Variable</h1>
        <p><?= $buildingObj -> name; ?></p>
        <p><?= $buildingObj->address->city." ".$buildingObj->address->country; ?></p>
        <?php var_dump($buildingObj)?>

        <h1>Object from Class</h1>
        <p><?php var_dump($building); ?></p>
        <p><?= $building->name; ?></p>
        <p><?= $building->printName(); ?> </p>
        <p><?= $building->checkFloors(); ?> </p>
        <p><?= $secondBuilding->printName(); ?></p>
        
        <h1>Inheritance</h1>
        <p><?php var_dump($condominium);?></p>
        <p><?= $condominium->printName(); ?></p>
        <p><?= $condominium->checkFloors(); ?></p>
        <p><?= $condominium->checkZipCode(); ?></p>

        <h1>Abstraction</h1>
        <p><?php var_dump($kopiko); ?></p>
        <p><?php echo $kopiko->getDrinkName(); ?></p>
    </body>

</html>